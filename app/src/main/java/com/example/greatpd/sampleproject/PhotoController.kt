package com.example.greatpd.sampleproject

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews

class PhotoController(context : Context){

    val mContext = context


    fun changesPhotos(views: RemoteViews) {
        var intents : Intent = Intent(Intent.ACTION_PICK)
        intents.setType("image/*");
        intents.setAction(Intent.ACTION_GET_CONTENT)

        val pendingIntents = PendingIntent.getActivity(mContext,0,intents,0)
        views.setOnClickPendingIntent(R.id.showGallery,pendingIntents)

        val widgetContoller = WidgetController(mContext)
        widgetContoller.updateWidget(views)
    }
}
