package com.example.greatpd.sampleproject

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log

var photoData: Bitmap? = null

class PhotoModel(context : Context){

    val mContext = context

    fun getPhotoData() : Bitmap? {
        Log.e("photoData====>", photoData.toString())
        return photoData
    }

    fun setPhotoData(intent: Intent?) {
        photoData  = changeBitmap(intent?.data)
        Log.e("setPhotoData====>", photoData.toString())
    }

    private fun changeBitmap(data: Uri?) :Bitmap{
        val inputstreams = mContext.contentResolver.openInputStream(data)
        var bitmapImage = BitmapFactory.decodeStream(inputstreams)
        inputstreams.close()
        return bitmapImage
    }

}