package com.example.greatpd.sampleproject

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.widget.RemoteViews

/**
 * Implementation of App Widget functionality.
 */
var mService: ComponentName? = null
val APPWIDGET_UPDATE: String? = "com.example.greatpd.sampleproject.APPWIDGET_UPDATE"

class NewAppWidgetView : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        Log.e("onUpdate====>", "onUpdate")
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }


    override fun onEnabled(context: Context) {
        Log.e("onEnabled====>", "onEnabled")
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        Log.e("onDisabled====>", "onDisabled")
        // Enter relevant functionality for when the last widget is disabled
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        Log.e("onReceive====>", "onReceive")
    }

    companion object {

        internal fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {

            val widgetText = context.getString(R.string.appwidget_text)
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.new_app_widget)
            views.setTextViewText(R.id.appwidget_text, widgetText)
            // Instruct the widget manager to update the widget

            val photoModel = PhotoModel(context)
            if(photoModel.getPhotoData() !=null){
                views.setImageViewBitmap(R.id.showGallery,photoModel.getPhotoData())
            }

            val photoContoller = PhotoController(context)
            photoContoller.changesPhotos(views)

            val widgetController = WidgetController(context)
            widgetController.btnUpdate(views)


            appWidgetManager.updateAppWidget(appWidgetId, views)
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) mService = context.startForegroundService(Intent(context,WidgetService::class.java))
            else context.startService(Intent(context,WidgetService::class.java))
            }

    }
}

