package com.example.greatpd.sampleproject

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.RemoteViews
import com.example.greatpd.sampleproject.NewAppWidgetView.Companion.updateAppWidget

val GALLERY_CODE = 2343
val TAG = "MainActivity"
var activity : MainActivity? = null

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setActivity()
        setPermission()
    }


    private fun setActivity() {
        activity = this
    }

    private fun setPermission() {
        val perMission = PermissionContoller(this, activity!!)
        perMission.setPermission()
    }

    fun selectwidgetPhoto() {
        var intent : Intent = Intent(Intent.ACTION_PICK)
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(intent, GALLERY_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == GALLERY_CODE){
            val photoModel = PhotoModel(this)
            Log.e("data===>", data.toString())
            photoModel.setPhotoData(data)
        }
    }
}
