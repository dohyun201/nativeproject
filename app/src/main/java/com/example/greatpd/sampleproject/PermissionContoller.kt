package com.example.greatpd.sampleproject

import android.Manifest
import android.app.Activity
import android.content.Context
import android.util.Log
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import java.util.ArrayList

class PermissionContoller(context : Context, activity: Activity){
    val mainContext = context


    val permissionListener : PermissionListener = object : PermissionListener {
        override fun onPermissionGranted() {
            afterGrantedPermission()
        }

        override fun onPermissionDenied(deniedPermissions: ArrayList<String>?) {
            Log.e(TAG,"권한필요")
        }
    }

    private fun afterGrantedPermission() {
        activity?.selectwidgetPhoto()
    }

    fun setPermission() {
        TedPermission.with(mainContext)
            .setPermissionListener(permissionListener)
            .setRationaleMessage("권한 필요하다")
            .setDeniedMessage("권한 > 허용해라")
            .setPermissions(Manifest.permission.CAMERA)
            .check()
    }

}