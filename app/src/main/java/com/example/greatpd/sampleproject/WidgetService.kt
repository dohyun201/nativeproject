package com.example.greatpd.sampleproject

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.support.v4.os.HandlerCompat.postDelayed
import android.widget.RemoteViews
import android.support.v4.os.HandlerCompat.postDelayed
import android.content.ComponentName
import android.appwidget.AppWidgetManager
import java.text.SimpleDateFormat
import java.util.*


var mHandler: Handler? = null
var preTime :Long = 1000L
var curTime = 0

class WidgetService: Service(), Runnable {

    override fun run() {
        //위젯이 업데이트 되었음을 알린다.
        val appWidgetManager = AppWidgetManager.getInstance(this)

        val views = RemoteViews(this.packageName, R.layout.new_app_widget)
        val time = System.currentTimeMillis()
        val dayTime = SimpleDateFormat("yyyy-mm-dd hh:mm:ss")
        views.setTextViewText(R.id.appwidget_text,dayTime.format(Date(time)))

//        val photoModel = PhotoModel(this)
//        views.setImageViewBitmap(R.id.showGallery,photoModel.getPhotoData())

        val testWidge = ComponentName(this, NewAppWidgetView::class.java!!)
        appWidgetManager.updateAppWidget(testWidge, views)
    }

    override fun onBind(p0: Intent?): IBinder? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate() {
        super.onCreate()
        startService(Intent(this,WidgetService::class.java))
        Log.e("WidgetService====>", "onCreate")
        mHandler = Handler()
        preTime = System.currentTimeMillis()//현재 시간을 이전 시간에 저장 후
        mHandler?.postDelayed(this, 1000)   //1초뒤에 서비스클래스의 run()함수가 호출되도록 한다.
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
        Log.e("WidgetService====>", "onStartCommand")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("WidgetService====>", "onDestroy")

    }
}