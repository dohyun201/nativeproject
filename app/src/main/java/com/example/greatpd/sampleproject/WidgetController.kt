package com.example.greatpd.sampleproject

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import android.content.ComponentName
import android.appwidget.AppWidgetManager
import android.util.Log
import android.widget.Toast


class WidgetController(context : Context){

    val mContext = context

    fun updateWidget(views: RemoteViews){
        val manager = AppWidgetManager.getInstance(mContext)
        val newAppWidgetView = NewAppWidgetView()
        newAppWidgetView.onUpdate(mContext, manager, manager.getAppWidgetIds(ComponentName(mContext, javaClass)))
    }

    fun btnUpdate(views: RemoteViews){
        val intents = Intent(APPWIDGET_UPDATE)
        val pendingIntents = PendingIntent.getBroadcast(mContext,0,intents,0)
        views.setOnClickPendingIntent(R.id.btnAddIMG,pendingIntents)
    }
}